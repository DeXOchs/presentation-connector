# PresentationConnector

A dotnet tool to connect a stream-like service with a presentation service. The stream service (or any entity behind; here: OpenLP) can send messages which can affect the presentation service (or any entity which presents something; here: OBS).
