﻿namespace presentation_connector
{
    public class PollMessage
    {
        public string CurrentItemID { get; }
        public bool BlankView { get; }

        public PollMessage(string currentItemID, bool blankView)
        {
            CurrentItemID = currentItemID;
            BlankView = blankView;
        }

        public override bool Equals(object reference)
        {
            if (reference == null || reference is not PollMessage message) return false;
            else return message.CurrentItemID.Equals(CurrentItemID) && message.BlankView == BlankView;
        }

        public override int GetHashCode() => (CurrentItemID.GetHashCode() + BlankView.ToString()).GetHashCode();
    }
}
