﻿using System.Xml.Serialization;

namespace presentation_connector
{
    public enum ItemType
    {
        [XmlEnum(Name = "")]
        UNKNOWN = 0,
        [XmlEnum(Name = "PRESENTATION")]
        PRESENTATION,
        [XmlEnum(Name = "IMAGE")]
        IMAGE,
        [XmlEnum(Name = "TEXT")]
        TEXT,
        [XmlEnum(Name = "SONG")]
        SONG,
        [XmlEnum(Name = "CUSTOM")]
        CUSTOM
    }
}
