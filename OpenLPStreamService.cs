﻿using Newtonsoft.Json.Linq;
using System;
using System.Net.Http;
using System.Threading;

namespace presentation_connector
{
    public sealed class OpenLPStreamService : IStreamService, IDisposable
    {
        private Exception lastException = null;
        private PollMessage lastPollMessage;
        private readonly CancellationTokenSource pullTokenSource = new();
        private readonly HttpClient client = new();

        public Exception LastException { get => lastException; }
        public event PollEventHandler OnPollUpdate;

        public OpenLPStreamService(OpenLPStreamServiceConfiguration configuration)
        {
            client.BaseAddress = new Uri($"http://{configuration.Address}/");
            client.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
        }

        public ItemType GetItemType(string itemID)
        {
            try
            {
                HttpResponseMessage response = client.GetAsync("api/service/list").Result;
                if (response.IsSuccessStatusCode)
                {
                    JArray serviceItems = JObject
                        .Parse(response.Content.ReadAsStringAsync(CancellationToken.None).Result)
                        .SelectToken("results.items")
                        .ToArray();

                    foreach (JToken serviceItem in serviceItems)
                    {
                        if (serviceItem.SelectToken("id").ToString().Equals(itemID)) return serviceItem.SelectToken("plugin").ToString() switch
                        {
                            "presentations" => ItemType.PRESENTATION,
                            "images" => ItemType.IMAGE,
                            "bibles" => ItemType.TEXT,
                            "songs" => ItemType.SONG,
                            _ => ItemType.CUSTOM
                        };
                    }
                }
                lastException = null;
            }
            catch (Exception e)
            {
                lastException = new Exception($"Error fetching and processing OpenLP service item type.");
                Console.WriteLine(lastException.Message + e.InnerException.Message);
            }
            return ItemType.UNKNOWN;
        }

        public void Listen() => ThreadPool.QueueUserWorkItem(
            new WaitCallback((object pullToken) => Poll((CancellationToken)pullToken)),
            pullTokenSource.Token);

        public void Ignore() => pullTokenSource.Cancel();

        public void Dispose()
        {
            Ignore();
            pullTokenSource.Dispose();
            client.Dispose();
        }

        private async void Poll(CancellationToken pullToken)
        {
            while (!pullToken.IsCancellationRequested)
            {
                HttpResponseMessage response;
                try
                {
                    response = await client.GetAsync("api/poll", CancellationToken.None);
                }
                catch (Exception e)
                {
                    lastException = new Exception($"Error polling OpenLP API.", e);
                    Console.WriteLine(lastException.Message + lastException.InnerException.Message);
                    continue;
                }

                if (response.IsSuccessStatusCode)
                {
                    JObject json;
                    try
                    {
                        json = JObject.Parse(await response.Content.ReadAsStringAsync(CancellationToken.None));
                    }
                    catch (Exception e)
                    {
                        lastException = new Exception($"Error processing OpenLP API response.", e);
                        Console.WriteLine(lastException.Message + lastException.InnerException.Message);
                        continue;
                    }

                    if (bool.TryParse((json.SelectToken("results.theme") ?? string.Empty).ToString(), out bool isBlank))
                    {
                        PollMessage pollMessage = new((json.SelectToken("results.item") ?? string.Empty).ToString(), isBlank);
                        if (!pollMessage.Equals(lastPollMessage))
                        {
                            OnPollUpdate(pollMessage);
                            lastPollMessage = pollMessage;
                        }
                    }
                }
                lastException = null;
                Thread.Sleep(50);
            }
        }
    }
}
