﻿using System;
using System.IO;
using System.Linq;
using System.Xml;
using System.Xml.Serialization;

namespace presentation_connector
{
    public abstract class XMLUtils<T>
    {
        public static T FromXML(string fileName)
        {
            DirectoryInfo appDirectory = GetAppRootDirectory();

            if (appDirectory != null)
            {
                FileInfo file = null;

                try
                {
                    file = appDirectory.GetFiles(fileName).FirstOrDefault();
                }
                catch (Exception e)
                {
                    Console.WriteLine("Error reading specified file." + e.Message);
                }

                if (file != null)
                {
                    try
                    {
                        return (T)new XmlSerializer(typeof(T)).Deserialize(XmlReader.Create(file.FullName));
                    }
                    catch (Exception e) 
                    {
                        Console.WriteLine("Error deserializing specified file. " + e.Message);
                    }
                }
            }

            return default;
        }

        public static bool InitXML(string fileName, T serializable)
        {
            DirectoryInfo appDirectory = GetAppRootDirectory();

            if (appDirectory != null)
            {
                FileStream file;

                try
                {
                    file = File.Create(appDirectory + $"//{fileName}");
                    new XmlSerializer(typeof(T)).Serialize(file, serializable);
                    file.Close();
                    return true;
                }
                catch (Exception e)
                {
                    Console.WriteLine("Error creating, writing or serializing object into file. " + e.Message);
                }
            }

            return false;
        }

        private static DirectoryInfo GetAppRootDirectory()
        {
            DirectoryInfo appDirectory;
            try
            {
                appDirectory = new(AppDomain.CurrentDomain.SetupInformation.ApplicationBase);
            }
            catch (Exception e)
            {
                Console.WriteLine("Error getting own Application Domain. The application root directory can not be retrieved. " + e.Message);
                return null;
            }

            while (!appDirectory.Name.Equals("presentation-connector") && appDirectory != null)
            {
                try
                {
                    appDirectory = appDirectory.Parent;
                }
                catch (System.Security.SecurityException e)
                {
                    Console.WriteLine("Can not access directory due to security reasons. The application root directory can not be retrieved. " + e.Message);
                    return null;
                }
            }
            return appDirectory;
        }
    }
}
