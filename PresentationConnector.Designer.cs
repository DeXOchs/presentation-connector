﻿
using System;
using System.Drawing;
using System.Timers;
using System.Windows.Forms;

namespace presentation_connector
{
    partial class PresentationConnector
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        private ItemType? lastItemType = null;
        private bool? lastBlankView = null;
        private bool? lastPresentationServiceActionSuccessful = null;
        private DateTime? lastStreamServiceSignal = null;

        private Image checkIcon;
        private Image crossIcon;
        private Image networkIcon;

        private void SetUIBindings()
        {
            this.checkIcon = this.statusIcons.Images[this.statusIcons.Images.IndexOfKey("checkIcon")];
            this.crossIcon = this.statusIcons.Images[this.statusIcons.Images.IndexOfKey("crossIcon")];
            this.networkIcon = this.statusIcons.Images[this.statusIcons.Images.IndexOfKey("networkIcon")];

            streamServiceIcon.BackgroundImage = networkIcon;
            presentationServiceIcon.BackgroundImage = networkIcon;

            System.Timers.Timer t = new System.Timers.Timer(1000);
            t.Elapsed += (object ob, ElapsedEventArgs e) => BeginInvoke((MethodInvoker)delegate {
                if (this.streamService.LastException == null)
                {
                    if (this.lastStreamServiceSignal == null)
                    {
                        streamServiceInformation.Text = "Stream Service never reached.";
                    }
                    else
                    {
                        streamServiceInformation.Text = "Stream Service works.";
                        streamServiceIcon.BackgroundImage = checkIcon;
                    }
                }
                else
                {
                    streamServiceInformation.Text = "Stream Service: " + this.streamService.LastException.Message;
                    streamServiceIcon.BackgroundImage = crossIcon;
                }
            });
            t.Start();
        }

        private void UpdateUI()
        {
            itemType.Visible = lastItemType != null;
            itemType.Text = lastItemType.ToString().ToUpper();

            blankView.Visible = lastBlankView != null;
            blankView.Text = lastBlankView.ToString().ToUpper();

            if (lastStreamServiceSignal != null)
            {
                streamServiceStatus.Text = $"Last Signal\n{lastStreamServiceSignal.Value.ToString("HH:mm:ss")}";
            }

            if (lastPresentationServiceActionSuccessful != null)
            {
                if (lastPresentationServiceActionSuccessful.Value)
                {
                    presentationServiceStatus.Text = "Last Call\nsuccessful";
                    presentationServiceIcon.BackgroundImage = checkIcon;
                }
                else
                {
                    presentationServiceStatus.Text = $"Last Call\nfailed ({DateTime.Now.ToString("HH:mm:ss")})";
                    presentationServiceIcon.BackgroundImage = crossIcon;
                }
            }
        }


        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PresentationConnector));
            this.mainLayout = new System.Windows.Forms.TableLayoutPanel();
            this.topLayout = new System.Windows.Forms.TableLayoutPanel();
            this.blankViewLayout = new System.Windows.Forms.TableLayoutPanel();
            this.blankView = new System.Windows.Forms.Label();
            this.blankViewLabel = new System.Windows.Forms.Label();
            this.itemTypeLayout = new System.Windows.Forms.TableLayoutPanel();
            this.itemTypeLabel = new System.Windows.Forms.Label();
            this.itemType = new System.Windows.Forms.Label();
            this.midLayout = new System.Windows.Forms.TableLayoutPanel();
            this.presentationServiceBox = new System.Windows.Forms.GroupBox();
            this.presentationServiceLayout = new System.Windows.Forms.TableLayoutPanel();
            this.presentationServiceIcon = new System.Windows.Forms.Panel();
            this.presentationServiceStatus = new System.Windows.Forms.Label();
            this.streamServiceBox = new System.Windows.Forms.GroupBox();
            this.streamServiceLayout = new System.Windows.Forms.TableLayoutPanel();
            this.streamServiceIcon = new System.Windows.Forms.Panel();
            this.streamServiceStatus = new System.Windows.Forms.Label();
            this.bottomLayout = new System.Windows.Forms.TableLayoutPanel();
            this.informationBox = new System.Windows.Forms.GroupBox();
            this.informationLayout = new System.Windows.Forms.TableLayoutPanel();
            this.presentationServiceInformation = new System.Windows.Forms.Label();
            this.streamServiceInformation = new System.Windows.Forms.Label();
            this.statusIcons = new System.Windows.Forms.ImageList(this.components);
            this.mainLayout.SuspendLayout();
            this.topLayout.SuspendLayout();
            this.blankViewLayout.SuspendLayout();
            this.itemTypeLayout.SuspendLayout();
            this.midLayout.SuspendLayout();
            this.presentationServiceBox.SuspendLayout();
            this.presentationServiceLayout.SuspendLayout();
            this.streamServiceBox.SuspendLayout();
            this.streamServiceLayout.SuspendLayout();
            this.bottomLayout.SuspendLayout();
            this.informationBox.SuspendLayout();
            this.informationLayout.SuspendLayout();
            this.SuspendLayout();
            // 
            // mainLayout
            // 
            this.mainLayout.AutoSize = true;
            this.mainLayout.ColumnCount = 1;
            this.mainLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.mainLayout.Controls.Add(this.topLayout, 0, 0);
            this.mainLayout.Controls.Add(this.midLayout, 0, 1);
            this.mainLayout.Controls.Add(this.bottomLayout, 0, 2);
            this.mainLayout.Dock = System.Windows.Forms.DockStyle.Fill;
            this.mainLayout.Location = new System.Drawing.Point(0, 0);
            this.mainLayout.Margin = new System.Windows.Forms.Padding(0);
            this.mainLayout.Name = "mainLayout";
            this.mainLayout.RowCount = 3;
            this.mainLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 35F));
            this.mainLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 35F));
            this.mainLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 30F));
            this.mainLayout.Size = new System.Drawing.Size(482, 353);
            this.mainLayout.TabIndex = 0;
            // 
            // topLayout
            // 
            this.topLayout.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(59)))), ((int)(((byte)(85)))), ((int)(((byte)(119)))));
            this.topLayout.ColumnCount = 2;
            this.topLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.topLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.topLayout.Controls.Add(this.blankViewLayout, 1, 0);
            this.topLayout.Controls.Add(this.itemTypeLayout, 0, 0);
            this.topLayout.Dock = System.Windows.Forms.DockStyle.Fill;
            this.topLayout.Location = new System.Drawing.Point(0, 0);
            this.topLayout.Margin = new System.Windows.Forms.Padding(0);
            this.topLayout.Name = "topLayout";
            this.topLayout.RowCount = 1;
            this.topLayout.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.topLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 125F));
            this.topLayout.Size = new System.Drawing.Size(482, 123);
            this.topLayout.TabIndex = 0;
            // 
            // blankViewLayout
            // 
            this.blankViewLayout.ColumnCount = 1;
            this.blankViewLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.blankViewLayout.Controls.Add(this.blankView, 0, 1);
            this.blankViewLayout.Controls.Add(this.blankViewLabel, 0, 0);
            this.blankViewLayout.Location = new System.Drawing.Point(241, 0);
            this.blankViewLayout.Margin = new System.Windows.Forms.Padding(0);
            this.blankViewLayout.Name = "blankViewLayout";
            this.blankViewLayout.Padding = new System.Windows.Forms.Padding(0, 20, 0, 20);
            this.blankViewLayout.RowCount = 2;
            this.blankViewLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 40F));
            this.blankViewLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 60F));
            this.blankViewLayout.Size = new System.Drawing.Size(241, 125);
            this.blankViewLayout.TabIndex = 1;
            // 
            // blankView
            // 
            this.blankView.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.blankView.BackColor = System.Drawing.Color.BurlyWood;
            this.blankView.Font = new System.Drawing.Font("Segoe UI Semibold", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.blankView.Location = new System.Drawing.Point(45, 64);
            this.blankView.Name = "blankView";
            this.blankView.Size = new System.Drawing.Size(150, 30);
            this.blankView.TabIndex = 2;
            this.blankView.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.blankView.Visible = false;
            // 
            // blankViewLabel
            // 
            this.blankViewLabel.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.blankViewLabel.AutoSize = true;
            this.blankViewLabel.Font = new System.Drawing.Font("Segoe UI", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.blankViewLabel.ForeColor = System.Drawing.SystemColors.Window;
            this.blankViewLabel.Location = new System.Drawing.Point(72, 20);
            this.blankViewLabel.Name = "blankViewLabel";
            this.blankViewLabel.Size = new System.Drawing.Size(96, 25);
            this.blankViewLabel.TabIndex = 0;
            this.blankViewLabel.Text = "Blank View";
            // 
            // itemTypeLayout
            // 
            this.itemTypeLayout.ColumnCount = 1;
            this.itemTypeLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.itemTypeLayout.Controls.Add(this.itemTypeLabel, 0, 0);
            this.itemTypeLayout.Controls.Add(this.itemType, 0, 1);
            this.itemTypeLayout.Location = new System.Drawing.Point(0, 0);
            this.itemTypeLayout.Margin = new System.Windows.Forms.Padding(0);
            this.itemTypeLayout.Name = "itemTypeLayout";
            this.itemTypeLayout.Padding = new System.Windows.Forms.Padding(0, 20, 0, 20);
            this.itemTypeLayout.RowCount = 2;
            this.itemTypeLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 40F));
            this.itemTypeLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 60F));
            this.itemTypeLayout.Size = new System.Drawing.Size(241, 125);
            this.itemTypeLayout.TabIndex = 0;
            // 
            // itemTypeLabel
            // 
            this.itemTypeLabel.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.itemTypeLabel.AutoSize = true;
            this.itemTypeLabel.Font = new System.Drawing.Font("Segoe UI", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.itemTypeLabel.ForeColor = System.Drawing.SystemColors.Window;
            this.itemTypeLabel.Location = new System.Drawing.Point(75, 20);
            this.itemTypeLabel.Name = "itemTypeLabel";
            this.itemTypeLabel.Size = new System.Drawing.Size(90, 25);
            this.itemTypeLabel.TabIndex = 0;
            this.itemTypeLabel.Text = "Item Type";
            // 
            // itemType
            // 
            this.itemType.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.itemType.BackColor = System.Drawing.Color.BurlyWood;
            this.itemType.Font = new System.Drawing.Font("Segoe UI Semibold", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.itemType.Location = new System.Drawing.Point(45, 64);
            this.itemType.Name = "itemType";
            this.itemType.Size = new System.Drawing.Size(150, 30);
            this.itemType.TabIndex = 1;
            this.itemType.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.itemType.Visible = false;
            // 
            // midLayout
            // 
            this.midLayout.ColumnCount = 2;
            this.midLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.midLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.midLayout.Controls.Add(this.presentationServiceBox, 1, 0);
            this.midLayout.Controls.Add(this.streamServiceBox, 0, 0);
            this.midLayout.Dock = System.Windows.Forms.DockStyle.Fill;
            this.midLayout.Location = new System.Drawing.Point(0, 123);
            this.midLayout.Margin = new System.Windows.Forms.Padding(0);
            this.midLayout.Name = "midLayout";
            this.midLayout.Padding = new System.Windows.Forms.Padding(0, 15, 0, 15);
            this.midLayout.RowCount = 1;
            this.midLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.midLayout.Size = new System.Drawing.Size(482, 123);
            this.midLayout.TabIndex = 1;
            // 
            // presentationServiceBox
            // 
            this.presentationServiceBox.Controls.Add(this.presentationServiceLayout);
            this.presentationServiceBox.Font = new System.Drawing.Font("Segoe UI", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.presentationServiceBox.Location = new System.Drawing.Point(261, 15);
            this.presentationServiceBox.Margin = new System.Windows.Forms.Padding(20, 0, 20, 0);
            this.presentationServiceBox.Name = "presentationServiceBox";
            this.presentationServiceBox.Size = new System.Drawing.Size(201, 93);
            this.presentationServiceBox.TabIndex = 1;
            this.presentationServiceBox.TabStop = false;
            this.presentationServiceBox.Text = "Presentation Service";
            // 
            // presentationServiceLayout
            // 
            this.presentationServiceLayout.ColumnCount = 2;
            this.presentationServiceLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 64F));
            this.presentationServiceLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.presentationServiceLayout.Controls.Add(this.presentationServiceIcon, 0, 0);
            this.presentationServiceLayout.Controls.Add(this.presentationServiceStatus, 1, 0);
            this.presentationServiceLayout.Dock = System.Windows.Forms.DockStyle.Fill;
            this.presentationServiceLayout.Location = new System.Drawing.Point(3, 26);
            this.presentationServiceLayout.Name = "presentationServiceLayout";
            this.presentationServiceLayout.RowCount = 1;
            this.presentationServiceLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.presentationServiceLayout.Size = new System.Drawing.Size(195, 64);
            this.presentationServiceLayout.TabIndex = 0;
            // 
            // presentationServiceIcon
            // 
            this.presentationServiceIcon.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.presentationServiceIcon.Dock = System.Windows.Forms.DockStyle.Fill;
            this.presentationServiceIcon.Location = new System.Drawing.Point(10, 10);
            this.presentationServiceIcon.Margin = new System.Windows.Forms.Padding(10);
            this.presentationServiceIcon.Name = "presentationServiceIcon";
            this.presentationServiceIcon.Size = new System.Drawing.Size(44, 44);
            this.presentationServiceIcon.TabIndex = 0;
            // 
            // presentationServiceStatus
            // 
            this.presentationServiceStatus.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.presentationServiceStatus.AutoSize = true;
            this.presentationServiceStatus.Location = new System.Drawing.Point(77, 20);
            this.presentationServiceStatus.Name = "presentationServiceStatus";
            this.presentationServiceStatus.Size = new System.Drawing.Size(105, 23);
            this.presentationServiceStatus.TabIndex = 1;
            this.presentationServiceStatus.Text = "Initializing ...";
            // 
            // streamServiceBox
            // 
            this.streamServiceBox.Controls.Add(this.streamServiceLayout);
            this.streamServiceBox.Font = new System.Drawing.Font("Segoe UI", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.streamServiceBox.Location = new System.Drawing.Point(20, 15);
            this.streamServiceBox.Margin = new System.Windows.Forms.Padding(20, 0, 20, 0);
            this.streamServiceBox.Name = "streamServiceBox";
            this.streamServiceBox.Size = new System.Drawing.Size(201, 93);
            this.streamServiceBox.TabIndex = 0;
            this.streamServiceBox.TabStop = false;
            this.streamServiceBox.Text = "Stream Service";
            // 
            // streamServiceLayout
            // 
            this.streamServiceLayout.ColumnCount = 2;
            this.streamServiceLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 64F));
            this.streamServiceLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.streamServiceLayout.Controls.Add(this.streamServiceIcon, 0, 0);
            this.streamServiceLayout.Controls.Add(this.streamServiceStatus, 1, 0);
            this.streamServiceLayout.Dock = System.Windows.Forms.DockStyle.Fill;
            this.streamServiceLayout.Location = new System.Drawing.Point(3, 26);
            this.streamServiceLayout.Name = "streamServiceLayout";
            this.streamServiceLayout.RowCount = 1;
            this.streamServiceLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.streamServiceLayout.Size = new System.Drawing.Size(195, 64);
            this.streamServiceLayout.TabIndex = 0;
            // 
            // streamServiceIcon
            // 
            this.streamServiceIcon.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.streamServiceIcon.Dock = System.Windows.Forms.DockStyle.Fill;
            this.streamServiceIcon.Location = new System.Drawing.Point(10, 10);
            this.streamServiceIcon.Margin = new System.Windows.Forms.Padding(10);
            this.streamServiceIcon.Name = "streamServiceIcon";
            this.streamServiceIcon.Size = new System.Drawing.Size(44, 44);
            this.streamServiceIcon.TabIndex = 0;
            // 
            // streamServiceStatus
            // 
            this.streamServiceStatus.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.streamServiceStatus.AutoSize = true;
            this.streamServiceStatus.Location = new System.Drawing.Point(77, 20);
            this.streamServiceStatus.Name = "streamServiceStatus";
            this.streamServiceStatus.Size = new System.Drawing.Size(105, 23);
            this.streamServiceStatus.TabIndex = 1;
            this.streamServiceStatus.Text = "Initializing ...";
            // 
            // bottomLayout
            // 
            this.bottomLayout.ColumnCount = 1;
            this.bottomLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.bottomLayout.Controls.Add(this.informationBox, 0, 0);
            this.bottomLayout.Dock = System.Windows.Forms.DockStyle.Fill;
            this.bottomLayout.Location = new System.Drawing.Point(3, 249);
            this.bottomLayout.Name = "bottomLayout";
            this.bottomLayout.Padding = new System.Windows.Forms.Padding(0, 10, 0, 10);
            this.bottomLayout.RowCount = 1;
            this.bottomLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.bottomLayout.Size = new System.Drawing.Size(476, 101);
            this.bottomLayout.TabIndex = 2;
            // 
            // informationBox
            // 
            this.informationBox.Controls.Add(this.informationLayout);
            this.informationBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.informationBox.Font = new System.Drawing.Font("Segoe UI", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.informationBox.Location = new System.Drawing.Point(20, 10);
            this.informationBox.Margin = new System.Windows.Forms.Padding(20, 0, 20, 0);
            this.informationBox.Name = "informationBox";
            this.informationBox.Size = new System.Drawing.Size(436, 81);
            this.informationBox.TabIndex = 1;
            this.informationBox.TabStop = false;
            this.informationBox.Text = "Information";
            // 
            // informationLayout
            // 
            this.informationLayout.ColumnCount = 1;
            this.informationLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.informationLayout.Controls.Add(this.presentationServiceInformation, 0, 1);
            this.informationLayout.Controls.Add(this.streamServiceInformation, 0, 0);
            this.informationLayout.Dock = System.Windows.Forms.DockStyle.Fill;
            this.informationLayout.Location = new System.Drawing.Point(3, 26);
            this.informationLayout.Name = "informationLayout";
            this.informationLayout.RowCount = 2;
            this.informationLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.informationLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.informationLayout.Size = new System.Drawing.Size(430, 52);
            this.informationLayout.TabIndex = 0;
            // 
            // presentationServiceInformation
            // 
            this.presentationServiceInformation.AutoSize = true;
            this.presentationServiceInformation.Dock = System.Windows.Forms.DockStyle.Fill;
            this.presentationServiceInformation.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.presentationServiceInformation.Location = new System.Drawing.Point(3, 26);
            this.presentationServiceInformation.Name = "presentationServiceInformation";
            this.presentationServiceInformation.Size = new System.Drawing.Size(424, 26);
            this.presentationServiceInformation.TabIndex = 1;
            // 
            // streamServiceInformation
            // 
            this.streamServiceInformation.AutoSize = true;
            this.streamServiceInformation.Dock = System.Windows.Forms.DockStyle.Fill;
            this.streamServiceInformation.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.streamServiceInformation.Location = new System.Drawing.Point(3, 0);
            this.streamServiceInformation.Name = "streamServiceInformation";
            this.streamServiceInformation.Size = new System.Drawing.Size(424, 26);
            this.streamServiceInformation.TabIndex = 0;
            // 
            // statusIcons
            // 
            this.statusIcons.ColorDepth = System.Windows.Forms.ColorDepth.Depth8Bit;
            this.statusIcons.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("statusIcons.ImageStream")));
            this.statusIcons.TransparentColor = System.Drawing.Color.Transparent;
            this.statusIcons.Images.SetKeyName(0, "checkIcon");
            this.statusIcons.Images.SetKeyName(1, "crossIcon");
            this.statusIcons.Images.SetKeyName(2, "networkIcon");
            // 
            // PresentationConnector
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(482, 353);
            this.Controls.Add(this.mainLayout);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "PresentationConnector";
            this.Text = "PresentationConnector";
            this.mainLayout.ResumeLayout(false);
            this.topLayout.ResumeLayout(false);
            this.blankViewLayout.ResumeLayout(false);
            this.blankViewLayout.PerformLayout();
            this.itemTypeLayout.ResumeLayout(false);
            this.itemTypeLayout.PerformLayout();
            this.midLayout.ResumeLayout(false);
            this.presentationServiceBox.ResumeLayout(false);
            this.presentationServiceLayout.ResumeLayout(false);
            this.presentationServiceLayout.PerformLayout();
            this.streamServiceBox.ResumeLayout(false);
            this.streamServiceLayout.ResumeLayout(false);
            this.streamServiceLayout.PerformLayout();
            this.bottomLayout.ResumeLayout(false);
            this.informationBox.ResumeLayout(false);
            this.informationLayout.ResumeLayout(false);
            this.informationLayout.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel mainLayout;
        private System.Windows.Forms.TableLayoutPanel topLayout;
        private System.Windows.Forms.TableLayoutPanel itemTypeLayout;
        private System.Windows.Forms.Label itemTypeLabel;
        private System.Windows.Forms.Label itemType;
        private System.Windows.Forms.TableLayoutPanel blankViewLayout;
        private System.Windows.Forms.Label blankViewLabel;
        private System.Windows.Forms.Label blankView;
        private System.Windows.Forms.TableLayoutPanel midLayout;
        private System.Windows.Forms.GroupBox presentationServiceBox;
        private System.Windows.Forms.TableLayoutPanel presentationServiceLayout;
        private System.Windows.Forms.Panel presentationServiceIcon;
        private System.Windows.Forms.Label presentationServiceStatus;
        private System.Windows.Forms.GroupBox streamServiceBox;
        private System.Windows.Forms.TableLayoutPanel streamServiceLayout;
        private System.Windows.Forms.Panel streamServiceIcon;
        private System.Windows.Forms.Label streamServiceStatus;
        private System.Windows.Forms.TableLayoutPanel bottomLayout;
        private System.Windows.Forms.GroupBox informationBox;
        private System.Windows.Forms.TableLayoutPanel informationLayout;
        private System.Windows.Forms.Label presentationServiceInformation;
        private System.Windows.Forms.Label streamServiceInformation;
        private ImageList statusIcons;
    }
}