﻿using System;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace presentation_connector
{
    [Serializable()]
    [XmlRoot(ElementName = "OBSPresentationServiceConfiguration")]
    public class OBSPresentationServiceConfiguration
    {
        public static OBSPresentationServiceConfiguration GetDummy()
            => new()
            {
                Address = "ws://ip:port",
                Credentials = "credentials",
                ItemTypeMappings = new List<ItemTypeMapping> {
                    new ItemTypeMapping
                    {
                        ItemType = ItemType.TEXT,
                        Targets = new List<ItemTypeMapping.ItemTypeMappingTarget>
                        {
                            new ItemTypeMapping.ItemTypeMappingTarget
                            {
                                SceneName = "text_scene_1",
                                SourceName = "text_source_1"
                            }
                        }
                    }
                }
            };

        [XmlElement(ElementName = "Address")]
        public string Address { get; set; }

        [XmlElement(ElementName = "Credentials")]
        public string Credentials { get; set; }

        [XmlArray("ItemTypeMappings")]
        [XmlArrayItem("Mapping", IsNullable = false)]
        public List<ItemTypeMapping> ItemTypeMappings { get; set; }

        [XmlRoot(ElementName = "Mapping")]
        public class ItemTypeMapping
        {
            [XmlAttribute(AttributeName = "itemType")]
            //[XmlElement(ElementName = "ItemType")]
            /// <summary>Use ItemType</summary>
            /// <see cref="ItemType"/>
            public string ItemTypeString
            {
                get => ItemType.ToString();
                set
                {
                    if (!Enum.IsDefined(typeof(ItemType), value )) ItemType = ItemType.UNKNOWN;
                    else ItemType = (ItemType)Enum.Parse(typeof(ItemType), value);
                }
            }

            [XmlIgnore]
            public ItemType ItemType { get; set; }
            
            [XmlArray("Targets")]
            [XmlArrayItem("Target", IsNullable = false)]
            public List<ItemTypeMappingTarget> Targets { get; set; }

            [XmlRoot(ElementName = "Target")]
            public class ItemTypeMappingTarget
            {
                [XmlAttribute(AttributeName = "sceneName")]
                public string SceneName { get; set; }

                [XmlAttribute(AttributeName = "sourceName")]
                public string SourceName { get; set; }
            }
        }
    }
}
