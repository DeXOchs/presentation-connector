﻿using Newtonsoft.Json.Linq;

namespace presentation_connector
{
    public static class Extensions
    {
        public static JArray ToArray(this JToken token)
            => JArray.Parse(token.ToString());
    }
}
